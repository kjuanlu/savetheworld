﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainLogic : MonoBehaviour
{

    public const float PLAYER_LIFE_VALUE = 1f;
    public const float WORLD_LIFE_VALUE = 1f;
    public const float TOTAL_TIME_GAME_VALUE = 200f;

    public enum GameStatus { Menu = 0, Playing = 1, End = 11 };

    [HideInInspector]
    public float playerLife;
    [HideInInspector]
    public float worldLife;
    [HideInInspector]
    public float countdown;

    private bool gameOver = false;

    public GameObject pnGameOver;
    public GameObject pnWin;
    public Button retryButtonGameOver;
    public Button menuButtonGameOver;
    public Button retryButtonWin;
    public Button menuButtonWin;
    public Text uiPlayerLifeText;
    public Image uiPlayerLifeBarImg;
    public Text uiWorldLifeText;
    public Image uiWorldBarImg;
    public Text uiCountdownText;


    protected GameStatus gameStatus;

    // Game Instance Singleton
    public static MainLogic instance;


    private void Awake()
    {
        instance = this;
        gameStatus = GameStatus.Menu;
    }

    void Start()
    {
        StartGame();
        //Testing
       //InvokeRepeating("TestingLogic", 5f, 1f);
    }

    public void StartGame()
    {
        gameOver = false;
        playerLife = PLAYER_LIFE_VALUE;
        worldLife = WORLD_LIFE_VALUE;
        countdown = TOTAL_TIME_GAME_VALUE;
        pnGameOver.SetActive(false);
        pnWin.SetActive(false);

        UpdateUI();
        if (AudioSingleton.Instance != null)
        {
            AudioSingleton.Instance.PlayAction();
        }
        //TODO Borramos todos los enemigos existentes
    }

    public void PlayerHit(float damange)
    {
       


        playerLife -= damange;
        if (playerLife < 0f)
        {
            GameOver();
        }
    }

    public void WorldHit(float damange)
    {
        Debug.Log("__MainLogic: World Hit");

        worldLife -= damange;
        if (worldLife < 0f)
        {
            GameOver();
        }
    }

    public void EnemyDead()
    {
       // Debug.Log("__MainLogic: Enemy Dead");
    }

    public void GameOver()
    {
        gameOver = true;
        //TODO: Desactivamos creacion de enemigos
        pnGameOver.SetActive(true);
        //retryButtonGameOver.onClick.AddListener(delegate () { RetryGame(); });
        //menuButtonGameOver.onClick.AddListener(delegate () { GoToMenu(); });
        //retryButtonWin.onClick.AddListener(delegate () { RetryGame(); });
        //menuButtonWin.onClick.AddListener(delegate () { GoToMenu(); });

    }

    public void RetryGame()
    {
        Debug.Log("retry");
        StartGame();
    }
    public void GoToMenu()
    {
        Debug.Log("menu");
        SceneManager.LoadScene((int) JumpToSceneForTesting.Scene.Start);
    }

    void TestingLogic()
    {

        PlayerHit(0.25f);
        Debug.Log("Hit Player " + playerLife);

    }

    void UpdateUI()
    {
        uiPlayerLifeText.text = playerLife.ToString();
        uiPlayerLifeBarImg.fillAmount = playerLife;
        uiWorldLifeText.text = worldLife.ToString();
        uiWorldBarImg.fillAmount = worldLife;

        TimeSpan time = TimeSpan.FromSeconds(countdown);
        uiCountdownText.text = time.ToString("mm':'ss");

    }

    void Update()
    {
        
        if (!gameOver)
        {
            countdown -= Time.deltaTime;
        }

        if (countdown < 0f)
        {

            pnWin.SetActive(true);
        }

        UpdateUI();
    }
}
