﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Global 
{

    public enum Layers { Player = 9, Enemy1 = 10, Floor = 11 };

    public static string Enemy1PrefabName = "Enemy1";
    public static string Enemy2PrefabName = "Enemy2";
    public static string Enemy3PrefabName = "Enemy3";

}
