﻿using UnityEngine;
using System.Collections;

public class AudioSingleton : MonoBehaviour
{
    private static AudioSingleton instance = null;
    public AudioSource musicAudioSource;
    public AudioClip musicAudioMenu;
    public AudioClip musicAudioAction;
    // Game Instance Singleton
    public static AudioSingleton Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);

        PlayMenu();
    }

   public void PlayAction()
    {
        musicAudioSource.clip = musicAudioAction;
        musicAudioSource.Play();
    }
    public void PlayMenu()
    {
        musicAudioSource.clip = musicAudioMenu;
        musicAudioSource.Play();
    }



}