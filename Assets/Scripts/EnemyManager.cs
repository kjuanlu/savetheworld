﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager instance;

    public Transform[] spawnPointsEnemy1;
    public float frecuencyEnemy1=0f;

    public Transform[] spawnPointsEnemy2;
    public float frecuencyEnemy2 = 0f;

    private Transform[] enemies2Active;

    int enemyCount1 = 0;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

        enemies2Active = new Transform[spawnPointsEnemy2.Length];

        InvokeRepeating("NewEnemy1", frecuencyEnemy1, frecuencyEnemy1);
        InvokeRepeating("NewEnemy3", frecuencyEnemy2, frecuencyEnemy2);

        //NewEnemy1();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void NewEnemy1()
    {
        if (enemyCount1 < 7)
        {
            enemyCount1++;
            Transform spawn = spawnPointsEnemy1[Random.Range(0, spawnPointsEnemy1.Length)];
            GameObject go = Instantiate(Resources.Load(Global.Enemy1PrefabName)) as GameObject;
            go.transform.position = spawn.position;
        }
    }

    void NewEnemy3()
    {
        if (enemyCount1 < 7)
        {
            enemyCount1++;
            Transform spawn = spawnPointsEnemy1[Random.Range(0, spawnPointsEnemy1.Length)];
            GameObject go = Instantiate(Resources.Load(Global.Enemy3PrefabName)) as GameObject;
            go.transform.position = spawn.position;
        }
    }

    void NewEnemy2()
    {
        //Esto es una mala implementacion ya que se basa en probailidad y se puede dar el caso en el que salgan todos o solo salga uno
        int index = Random.Range(0, spawnPointsEnemy2.Length);
        Transform spawn = spawnPointsEnemy2[index];

        if (enemies2Active[index] == null) 
        {
            GameObject go = Instantiate(Resources.Load(Global.Enemy2PrefabName)) as GameObject;
            go.transform.position = spawn.position;
            Enemy2Controller enemy2 = go.GetComponent<Enemy2Controller>();
            enemy2.initPosition = spawn.position;
            enemies2Active[index] = go.transform;
        }
        

    }

    public void DestroyEnemy1()
    {
        enemyCount1--;
    }

    void DestroyEnemy2()
    {

    }

}
