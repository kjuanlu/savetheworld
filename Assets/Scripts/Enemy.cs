﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public bool isWalking;
    public virtual void Damage()
    {
        MainLogic.instance.EnemyDead();
        gameObject.SetActive(false);

    }
}
