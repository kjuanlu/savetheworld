﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Controller : Enemy
{

    private Rigidbody rigidbodyEnemy;
    private bool isWalking = false;


    public float walkXMaxSpeed=900f;
    public float walkAceleration=500f;
    //Maxima distancia desde el centro en unidades
    public float maxDistance = 2f;

    [HideInInspector]
    public Vector3 initPosition;
    private bool goToRightSense = true;
    // Start is called before the first frame update
    void Start()
    {
        rigidbodyEnemy = GetComponent<Rigidbody>();
        

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isWalking)
        {
            WalkToTarget();
        }


    }


    void WalkToTarget()
    {
        Vector3 distance = initPosition - transform.position;
   

       
        if ((rigidbodyEnemy.velocity.x < walkXMaxSpeed || rigidbodyEnemy.velocity.x > -walkXMaxSpeed))
        {
            //va a la derecha
            if (goToRightSense)
            {
                rigidbodyEnemy.velocity += Vector3.right * walkAceleration * Time.fixedDeltaTime;

               if( transform.position.x > initPosition.x + maxDistance && goToRightSense)
                {
                    goToRightSense = false;
                }
            }


            //va a la izq
            if (!goToRightSense)
            {
                rigidbodyEnemy.velocity += Vector3.left * walkAceleration * Time.fixedDeltaTime;

                if (transform.position.x < initPosition.x - maxDistance && !goToRightSense)
                {
                    goToRightSense = true;
                }
            }

        }

    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.layer == (int) Global.Layers.Floor)
        {
            isWalking = true;
        }
    }

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.layer == (int) Global.Layers.Floor)
        {
            isWalking = false;
        }
    }
}
