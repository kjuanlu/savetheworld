﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1Controller : Enemy
{

    public bool followWorldBall = false;

    private CapsuleCollider s_collider;
    private Rigidbody rigidbodyEnemy;
   // private bool isWalking = false;


    public float walkXMaxSpeed=700f;
    public float walkAceleration=500f;
    public GameObject particle;

     public Transform target;
    // Start is called before the first frame update




    public override void Damage()
    {
        EnemyDeadSound.instance.PlayDeadSound();

        if (particle != null)
        {
            particle.SetActive(true);
            particle.transform.parent = null;
        }

        EnemyManager.instance.DestroyEnemy1();
        MainLogic.instance.EnemyDead();
        gameObject.SetActive(false);

    }



    void Start()
    {
        rigidbodyEnemy = GetComponent<Rigidbody>();
        s_collider = GetComponent<CapsuleCollider>();

        //for testing
        if (followWorldBall)
        {
            target = PlayerController.instance.transform;
        }
        else  
        {
            //target = GameObject.FindGameObjectWithTag("Player").transform;
            target = SphereWorld.instance.transform;
            
        }

        s_collider.gameObject.layer = 10;
        //Physics.IgnoreLayerCollision(11, 10, false);

        walkXMaxSpeed *= Random.Range(0.8f, 1.2f);
    }

    //Physics.IgnoreLayerCollision(11, 10, false);

    // Update is called once per frame
    void FixedUpdate()
    {

        Debug.DrawRay(transform.position - (Vector3.up * s_collider.height * 0.45f), -Vector3.up* 0.2f, Color.red);

        if(rigidbodyEnemy.velocity.y > 0)
        {
            s_collider.gameObject.layer = 15;
            //Physics.IgnoreLayerCollision(11, 10, true);
        }
        else
        {
            s_collider.gameObject.layer = 10;
        }

        if (Physics.Raycast(transform.position - (Vector3.up * s_collider.height * 0.45f), -Vector3.up, 0.2f, (1 << 11)))
        {
            if (rigidbodyEnemy.velocity.y <= 0)
            {
                isWalking = true;
                s_collider.gameObject.layer = 10;
                //Physics.IgnoreLayerCollision(11, 10, false);
            }
        }
        else
        {
            isWalking = false;
            s_collider.gameObject.layer = 15;
            //Physics.IgnoreLayerCollision(11, 10, true);
        }


        if (isWalking)
        {
            WalkToTarget();
        }
        
        //Debug.DrawRay(transform.position, Vector3.down);
    }

    void UpdateNoWalk()
    {

    }


    //Esta función se llama sólo si está andando en el suelo
    void WalkToTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Vector3 horizontal_direction = new Vector3(direction.x, -0.1f, direction.z);
        horizontal_direction *= walkAceleration;

        //Jump. Nada más saltar dejará de estar en el suelo, con lo cual esta función no volverá a llamarse hasta que toque nuevamente el suelo.
        if (direction.y > 0.3f && direction.y < 0.6f && isWalking)
        {
        //    Physics.IgnoreLayerCollision(11, 10, true);
            horizontal_direction.y = 400f;
            isWalking = false;
        }

        Debug.DrawRay(transform.position, horizontal_direction, Color.green);

        //if (rigidbodyEnemy.velocity.x < walkXMaxSpeed || rigidbodyEnemy.velocity.x > -walkXMaxSpeed)
        //{
            //Los enemigos se quedan pillados en el techo porque intentan ir en linea recta hacia él
            //rigidbodyEnemy.velocity = (direction * walkAceleration * Time.fixedDeltaTime);
            rigidbodyEnemy.velocity = (horizontal_direction * Time.fixedDeltaTime);

        //}
    }

    //void OnCollisionEnter(Collision col)
    //{
    //    if (col.gameObject.layer == (int) Global.Layers.Floor)
    //    {            

    //        if (Physics.Raycast(new Ray(transform.position,Vector3.down)))
    //        {

    //            isWalking = true;

    //        }
               
    //    }
    //}

    //void OnCollisionExit(Collision col)
    //{
        //if (col.gameObject.layer == (int) Global.Layers.Floor)
        //{
        //    isWalking = false;
        //}
    //}
}
