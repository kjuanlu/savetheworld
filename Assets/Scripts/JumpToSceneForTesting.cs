﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JumpToSceneForTesting : MonoBehaviour
{

    public enum Scene { Start = 0, Selection = 1, Game = 2};

    public Scene scene;
    public CanvasGroup canvasUIForSelection;
    private float timeFade = 1f;
    private bool fading = false;

    void Start()
    {
        Button b = gameObject.GetComponent<Button>();
        b.onClick.AddListener(delegate () { StartGame(); });
    }

    public void StartGame()
    {

        if (scene == Scene.Selection)
        {
            fading = true;
        }
        else
        {
            SceneManager.LoadScene((int)scene);
        }
    }

    void Update()
    {
        if (fading)
        {
            canvasUIForSelection.alpha -= Time.deltaTime;
            if (canvasUIForSelection.alpha <= 0)
            {
                SceneManager.LoadScene((int)scene);
            }
        }
    }

}
