﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlayer : MonoBehaviour
{
    private Rigidbody rigidbodyPlayer;
    private Transform transformPlayer;


    public float jumpForce = 9000f;
    public float walkForce = 100f;
    public float maxLateralPlayerSpeed = 20f;

    // Start is called before the first frame update
    void Start()
    {
        rigidbodyPlayer = (Rigidbody) GetComponent<Rigidbody>();
        transformPlayer = (Transform) GetComponent<Transform>();
    }


    void Update()
    {

    }
    void FixedUpdate()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbodyPlayer.AddForce(Vector3.up* jumpForce * Time.fixedDeltaTime);
        }

        if (Input.GetKey(KeyCode.A) && rigidbodyPlayer.velocity.x > -maxLateralPlayerSpeed)
        {
            rigidbodyPlayer.AddForce(Vector3.left * walkForce * Time.fixedDeltaTime);
        }

        if (Input.GetKey(KeyCode.D) && rigidbodyPlayer.velocity.x < maxLateralPlayerSpeed)
        {
            rigidbodyPlayer.AddForce(Vector3.right * walkForce * Time.fixedDeltaTime);
        }
        

    }
}

