﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimator : MonoBehaviour
{
    Enemy enemy;
    public Animator animator;
    public Transform graphicTransform;
    private Rigidbody mRigidBody;

    int walkingParam;

    // Start is called before the first frame update
    void Start()
    {

        enemy = GetComponent<Enemy>();
        mRigidBody = GetComponent<Rigidbody>();

        walkingParam = Animator.StringToHash("walking");
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 horizonatlVel = mRigidBody.velocity;
        horizonatlVel.y = 0;
        if (horizonatlVel != Vector3.zero)
        {
            graphicTransform.rotation = Quaternion.Lerp(graphicTransform.rotation, Quaternion.LookRotation(horizonatlVel, Vector3.up), Time.deltaTime * 10.0f);
        }

        //Mathf.Abs(mRigidBody.velocity.y) > 0.1f && 
        if (enemy.isWalking)
        {
            animator.SetBool(walkingParam, false);
        }
        else
        {
            animator.SetBool(walkingParam, true);
        }
    }
}
