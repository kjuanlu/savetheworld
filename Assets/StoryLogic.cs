﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoryLogic : MonoBehaviour
{
    private float timeZoom = 0f;

    public RectTransform[] story;

    private int currentStory;

    // Start is called before the first frame update
    void Start()
    {
        int index = 0;

        while (index != story.Length)
        {
            if (index == 0)
            {
                story[index].gameObject.SetActive(true);
            }
            else
            {
                story[index].gameObject.SetActive(false);
            }
            story[index].localScale = Vector3.one;

            index++;
        }

        currentStory = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentStory != story.Length)
        {
            

            if (timeZoom < 5f)
            {
                RectTransform rect = story[currentStory];
                rect.localScale += Vector3.one * Time.deltaTime * 0.05f;
                story[currentStory] = rect;// por si acaso xD
                timeZoom += Time.deltaTime;
            }
            else
            {
                if (currentStory != story.Length -1) { story[currentStory].gameObject.SetActive(false); }
                timeZoom = 0;
                currentStory++;
                if (currentStory < story.Length)
                {
                    story[currentStory].gameObject.SetActive(true);
                }
            }


           
        }
        else
        {
            SceneManager.LoadScene((int)JumpToSceneForTesting.Scene.Game);
        }
    }
}
