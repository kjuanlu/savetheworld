﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereWorld : MonoBehaviour
{
    float _time;
    Rigidbody mRigidbody;
    SphereCollider collider;

    
    Transform currentPoint;

    public static SphereWorld instance;

    private void Awake()
    {
        instance = this;

    }
    // Start is called before the first frame update
    void Start()
    {
        collider = GetComponent<SphereCollider>();
        GetRandomPoint();
        mRigidbody = GetComponent<Rigidbody>();
        mRigidbody.AddForce(Vector3.right * 100f);

       
    }

    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime;
        if(_time > 5f)
        {
            _time = 0;
            GetRandomPoint();
        }
    }

    void GetRandomPoint()
    {
      
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.contacts[0].otherCollider.gameObject.layer == 13)
        {
            float dotV = Vector3.Dot(Vector3.right, collision.contacts[0].normal);
            if (Mathf.Abs(dotV) > 0.7f)
            {
              
                mRigidbody.velocity = new Vector3(dotV * 2, mRigidbody.velocity.y, mRigidbody.velocity.z);

            }

        }

        else
        {
            

            if (collision.impulse.y > 18)
            {
                collider.material.bounciness = Random.Range(0.8f, 1);
            }
            if (collision.impulse.y < 13)
            {
                mRigidbody.AddForce(Vector3.up * Random.Range(100f, 300f));
            }
        }
        //Random bounciness
        //float bouncinesRandom = Random.Range(-100f, 100f);
        //if (bouncinesRandom < 0)
        //{
        //    collider.material.bounciness = 0.8f;
        //}
        //else
        //{
        //    collider.material.bounciness = 1f;
        //}
        //mRigidbody.AddForce(Vector3.up * bouncinesRandom);
       


    }


    private void FixedUpdate()
    {

        mRigidbody.angularVelocity = new Vector3(4, 1, 3);

        if (mRigidbody.velocity.y > 0)
        {
            //mRigidbody.detectCollisions = false;
            Physics.IgnoreLayerCollision(11, 12, true);
        }
        else
        {
           
            if (Physics.Raycast(transform.position- Vector3.up*collider.radius, Vector3.up * -1f, 0.4f, (1 << 11)))
            {
                Physics.IgnoreLayerCollision(11, 12, false);
                //mRigidbody.detectCollisions = true;
            }
        }

      
        //Clamp velocity
        if(mRigidbody.velocity.y < -10)
        {
            mRigidbody.velocity = new Vector3(mRigidbody.velocity.x, -10, mRigidbody.velocity.z);
        }

        if (mRigidbody.velocity.x < -10)
        {
            mRigidbody.velocity = new Vector3(-10, mRigidbody.velocity.y, mRigidbody.velocity.z);
        }

        if (mRigidbody.velocity.x > 10)
        {
            mRigidbody.velocity = new Vector3(10, mRigidbody.velocity.y, mRigidbody.velocity.z);
        }
        //if(mRigidbody.velocity.y > )
    }


}
