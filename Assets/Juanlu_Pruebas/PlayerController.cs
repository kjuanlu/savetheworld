﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    #region configurable_player
    public float horizontal_speed = 1.0f;
    public float jumpForce = 10.0f;
    public float gravity = 5.0f;
    public float maxFallingSpeed = -15.0f;
    [Range(4f,20)]
    public float airControll = 10.0f;
    public Transform graphicTransform;
    #endregion


    #region Input
    float horizontalAxes, last_horizontalAxes;
    bool jumpButton = false;
    bool bendButton = false;
    #endregion


    bool isBend = false;
    Rigidbody controller;
    CapsuleCollider charCtrl;
    Vector3 moveDirection;
    float velocityJump;
    float heighCollider;

    float resetGroundTime;
    float jumpGroundTime;
    bool isGrounded = false;
    public bool isFiring = false;

    public Animator animator;
    int idleAnim;
    int runAnim;
    int jumpAnim;

    int speedParam;
    int jumpParam;
    int gravityParam;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {

        controller = GetComponent<Rigidbody>();
        charCtrl = GetComponent<CapsuleCollider>();

        heighCollider = charCtrl.height;
        bendButton = false;
        isGrounded = false;

        idleAnim = Animator.StringToHash("Idle");
        runAnim = Animator.StringToHash("Run");
        jumpAnim = Animator.StringToHash("Jump");

        speedParam = Animator.StringToHash("speed");
        jumpParam = Animator.StringToHash("jump");
        gravityParam = Animator.StringToHash("gravity");


        animator.SetFloat(speedParam, 0);
        animator.SetBool(jumpParam, false);
    }

  
    void Update()
    {
        UpdateIsGround();
        GetInput();
        //Bend(); //Agacharse
        CalculateMoveDirection();
        UpdateGraphicTransform();
        ResetInput();

        AnimatorUpdate();
    }


    void AnimatorUpdate()
    {
        animator.SetFloat(speedParam, Mathf.Abs(horizontalAxes) );
        animator.SetBool(jumpParam, !isGrounded);
        animator.SetFloat(gravityParam, controller.velocity.y);
    }

    void CalculateMoveDirection()
    {

        //En el suelo
        if(isGrounded)
        {
            if (jumpButton)
            {
                if (jumpGroundTime > 0.2f)
                {
                    jumpGroundTime = 0;
                    velocityJump = jumpForce;
                }
                
            }
            else
            {
                velocityJump = 0;
            }

            last_horizontalAxes = horizontalAxes;

        }

        //Saltando o caida libre
        else
        {
            
            velocityJump -= gravity * Time.deltaTime;
            velocityJump = Mathf.Clamp(velocityJump, maxFallingSpeed, jumpForce);
        }
        


        //Movimiento Horizonntal
        moveDirection.z = 0;
        if (isGrounded)
        {
            moveDirection.x = horizontalAxes;
        }
        //Dificultad para controlar al personaje en el aire
        else
        {
            last_horizontalAxes += horizontalAxes * airControll * Time.deltaTime;
            last_horizontalAxes = Mathf.Clamp(last_horizontalAxes, -1, 1);
          
            moveDirection.x = last_horizontalAxes;
        }
        if (isGrounded && bendButton)
        {
            moveDirection *= horizontal_speed*0.5f;
        }
        else
        {
            moveDirection *= horizontal_speed;
        }


        moveDirection.y = velocityJump;
    }

    void Bend()
    {
        if (isGrounded && bendButton)
        {
            if (charCtrl.height == heighCollider)
            {
                isBend = true;
                charCtrl.height = 0.5f;
                transform.position -= Vector3.up * charCtrl.height*0.5f;
                graphicTransform.localPosition = new Vector3(0, -0.25f, 0);
                //Debug.Break();
            }
        }
        else
        {
            isBend = false;
            charCtrl.height = heighCollider;
            graphicTransform.localPosition = new Vector3(0, -0.5f, 0);
        }
    }

    public bool IsBend() { return isBend; }

    void GetInput()
    {

        horizontalAxes = Input.GetAxisRaw("Horizontal");

        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpButton = true;
        }

        //if(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        //{
        //    bendButton = true;
        //}
        //else
        //{
        //    bendButton = false;
        //}
    }

    void ResetInput()
    {
        jumpButton = false;
       
    }
   
  
    private void FixedUpdate()
    {

        controller.AddForce(((moveDirection - controller.velocity)), ForceMode.VelocityChange);

    }






    void UpdateIsGround()
    {

        jumpGroundTime += Time.deltaTime;

        if (controller.velocity.y > 0)
        {
            controller.detectCollisions = false;
            resetGroundTime = 0;
            isGrounded = false;
            return;
        }
        else
        {
            controller.detectCollisions = true;
        }

        if (jumpGroundTime < 0.2f)
        {
            resetGroundTime = 0;
            isGrounded = false;
            return;
        }

       

        //p1_ground is the lowest point of Capusle collider
        Vector3 p1 = transform.position + Vector3.up * (-charCtrl.height * 0.5f + charCtrl.radius + charCtrl.center.y);
        if (Physics.SphereCast(p1, charCtrl.radius * 0.9f, Vector3.up * -1, out RaycastHit hitInfo, charCtrl.radius * 0.25f, (1 << 11)))
        {
            resetGroundTime = 0;
            isGrounded = true;
            return;
        }

        else
        {
            //ResetGround
            resetGroundTime += Time.deltaTime;
            if (resetGroundTime > 0.05f)
            {
                isGrounded = false;
            }
        }


    }

    void UpdateGraphicTransform()
    {
        //Vector3 horizontalVelocity = controller.velocity;
        Vector3 horizontalVelocity = new Vector3(horizontalAxes, 0, 0)
        {
            y = 0
        };

        


        if (horizontalVelocity != Vector3.zero)
        {
            graphicTransform.rotation = Quaternion.Lerp(graphicTransform.rotation, Quaternion.LookRotation(horizontalVelocity, Vector3.up), Time.deltaTime * 10);
        }
        else if (isGrounded)
        {
            if (isFiring)
            {
                float dot = Vector3.Dot(graphicTransform.forward, Vector3.right);
                int side = dot > 0 ? 1 : -1;
                Quaternion rot = Quaternion.LookRotation(Vector3.right * side, Vector3.up);

                graphicTransform.rotation = Quaternion.Lerp(graphicTransform.rotation, rot, Time.deltaTime * 20);
            }
            else
            {
                graphicTransform.rotation = Quaternion.Lerp(graphicTransform.rotation, Quaternion.LookRotation(Vector3.forward * -1, Vector3.up), Time.deltaTime * 10);
            }
        }


        //Temporal, feedback de que se ha agachado
        if (isGrounded && bendButton)
        {
            graphicTransform.transform.localScale = new Vector3(1, 0.5f, 1);
        }
        else 
        {
            graphicTransform.transform.localScale = new Vector3(1, 1, 1);
        }

    }

}

