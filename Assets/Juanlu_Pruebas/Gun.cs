﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    private PlayerController controller;
    public Transform graphicTransform;
    public GameObject BulletPrefab;





    public GameObject[] pool_BulletPrefab;
    float timeFiring = 0;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<PlayerController>();

        pool_BulletPrefab = new GameObject[20];
        for(int i = 0; i < pool_BulletPrefab.Length; i++)
        {
            pool_BulletPrefab[i] = Instantiate(BulletPrefab, Vector3.zero, Quaternion.identity, null);
            pool_BulletPrefab[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.K))
        {
            Fire();
            controller.isFiring = true;
            timeFiring = 0.25f;
        }

        timeFiring -= Time.deltaTime;
        if(timeFiring <= 0)
        {
            controller.isFiring = false;
        }
    }

    void Fire()
    {
        float offset = controller.IsBend() ? -0.1f : 0.24f;
        Vector3 pos = transform.position + Vector3.up * offset;
        pos -= transform.forward * 0.1f;

        float dot = Vector3.Dot(graphicTransform.forward, Vector3.right);
        int side = dot > 0 ? 1 : -1;
        Quaternion rot = Quaternion.LookRotation(Vector3.right * side, Vector3.up);


        //GameObject bullet = Instantiate(BulletPrefab, pos, rot, null);
        GameObject bullet = GetBullet();
        bullet.transform.position = pos;
        bullet.transform.rotation = rot;
        bullet.SetActive(true);


    }



    GameObject GetBullet()
    {
        for(int i = 0; i < pool_BulletPrefab.Length; i++)
        {
            if (!pool_BulletPrefab[i].activeInHierarchy)
            {
                return pool_BulletPrefab[i];
            }
        }

        return Instantiate(BulletPrefab, Vector3.zero, Quaternion.identity, null);
    }
}
