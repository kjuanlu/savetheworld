﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    public float speed = 40.0f;
    float lifeTime = 0;

    private void OnEnable()
    {
        lifeTime = 0;
        hitted = false;
    }

    bool hitted = false;
    // Update is called once per frame
    void Update()
    {
        lifeTime += Time.deltaTime;
        if(lifeTime > 1.5f)
        {
            DestroyBullet();
        }

        if (!hitted)
        {
            transform.position += transform.forward * Time.deltaTime * speed;
        }

        DetectCollisionRaycast();
    }


    void DetectCollisionRaycast()
    {
        Debug.DrawRay(transform.position - transform.forward * 0.3f, transform.forward * 1f,Color.green);
        

        if( Physics.Raycast(transform.position - transform.forward*0.3f,  transform.forward,out RaycastHit hit, 1f))
        {
            if (hit.collider.GetComponentInParent<Enemy>())
            {
                //Debug.Log("Hit Enemy");
                hit.collider.GetComponentInParent<Enemy>().Damage();
                DestroyBullet();
            }
            if(hit.collider.gameObject.layer == (int)Global.Layers.Floor)
            {
                //Debug.Log("Hit Floor " + hit.collider.gameObject.name);
                DestroyBullet();
            }
        }
       
    }

    void DestroyBullet()
    {
        hitted = true;
        gameObject.SetActive(false);
    }
}
