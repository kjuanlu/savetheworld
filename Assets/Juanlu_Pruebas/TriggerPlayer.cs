﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPlayer : MonoBehaviour
{

    public bool isWorldSpehere = false;

    public Renderer damageRender;
    float hitTime;
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.layer == (int)Global.Layers.Enemy1)
        {
            if (hitTime <= 0)
            {
                if (isWorldSpehere)
                {
                    MainLogic.instance.WorldHit(0.1f);
                }
                else
                {
                    MainLogic.instance.PlayerHit(0.3f);
                }
                hitTime = 2.5f;
                damageRender.material.color = Color.red;
            }

        }

    }


    private void Update()
    {
        if (hitTime > 0)
        {
            hitTime -= Time.deltaTime;
        }
        else if (hitTime > -100 && hitTime < 0)
        {
            hitTime = -100;
            damageRender.material.color = Color.white;
        }
    }

}
