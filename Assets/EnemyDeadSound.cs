﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeadSound : MonoBehaviour
{
    public static EnemyDeadSound instance;
    public AudioSource source;
    // Start is called before the first frame update

    private void Awake()
    {
        instance = this;

    }

    public void PlayDeadSound()
    {
        source.PlayOneShot(source.clip);
    }
}
